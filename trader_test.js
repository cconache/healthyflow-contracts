var trader = require("./build/contracts/Trader.json");
var contract = require("truffle-contract");

const web3 = require('web3')

var provider = new web3.providers.HttpProvider("http://localhost:7545");

async function main() {

    var Trader = contract(trader);
    Trader.setProvider(provider)

    console.log(Trader);

    let cont =await Trader.at('0x0d8cc4b8d15d4c3ef1d70af0071376fb26b5669b');
    console.log(cont);
}

main();