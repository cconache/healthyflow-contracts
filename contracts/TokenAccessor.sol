pragma solidity ^0.4.17;

import "./Owned.sol";
import "./HFToken.sol";

contract TokenAccessor is Owned {

    HFToken public token;

    constructor() Owned() public {
    }

    function setToken( address token_address ) public onlyOwner {
        token = HFToken(token_address);  
    }
}