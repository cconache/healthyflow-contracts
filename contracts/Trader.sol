pragma solidity ^0.4.22;

import "./usingOraclize.sol";
import "./TokenAccessor.sol";

contract Trader is TokenAccessor, usingOraclize {
    
    // Define variables
    mapping(bytes32 => address ) buyer;
    mapping(bytes32 => string ) requestId;
    mapping(bytes32 => uint256 ) transactionAmount;
    mapping(bytes32 => bool) validIds;
    uint constant gasLimitForOraclize = 600000;
    
    // Events used to track contract actions
    event LogTransfer(string message );
    event LogResponseReceived(string response);
    event LogRequestSent(string message);
    event LogErrorResponse(string response);
    event LogBalance(uint256 balance);
    event LogPaidRequest(address buyer, string requestId ); // -- just this one is used

    function Trader() TokenAccessor() public {

        oraclize_setProof(proofType_TLSNotary | proofStorage_IPFS);      
        oraclize_setCustomGasPrice(1000000000 wei); // 1 Gwei
    }
           
    function buyRecord( string encryptedRecordData, uint256 amount, string request_id ) public payable {

        // require(msg.value >= 0.00006 ether);
        require( amount >= 0 );
        require( token.balanceOf(msg.sender) >= amount );
        
        bytes32 queryId = oraclize_query( 
          "nested", 
          getURL(encryptedRecordData,amount)
        );       

        emit LogRequestSent(encryptedRecordData);
        emit LogRequestSent("Payment query was sent, standing by for the answer..");
      
        // add query ID to mapping
        validIds[queryId] = true;
        
        buyer[queryId] = msg.sender;
        requestId[queryId] = request_id;
        transactionAmount[queryId] = amount;
        token.lockAccount( msg.sender );

    }
    
    // Callback function for Oraclize once it retreives the data 
    function __callback(bytes32 queryId, string result, bytes proof) public {
        require(msg.sender == oraclize_cbAddress());
        // validate the ID 
        require(validIds[queryId]);
        validIds[queryId] = false;

        token.unlockAccount( buyer[queryId] );        
        emit LogResponseReceived( result );

        address receiver = parseAddr(result);
        emit LogTransfer("Starting transfer...");
        token.transferFrom(buyer[queryId], receiver, transactionAmount[queryId]);
        emit LogPaidRequest( buyer[queryId], requestId[queryId] );

    }

    function getURL( string encryptedPayload, uint256 amount ) public returns(string) {
        
        string memory urlStart = "[URL] ['json(https://5y3p7ymt8g.execute-api.us-west-2.amazonaws.com/prod/pay).receiver', '{\"encrypted\": \"${[decrypt] ";
        string memory amountKey = "}\", \"amount\": ";
        string memory amountValue = uint2str(amount);
        string memory urlEnd = "${[identity] \"} \"}']";

        return strConcat(urlStart,encryptedPayload,amountKey,amountValue,urlEnd);
    }
    
}