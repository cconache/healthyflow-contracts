pragma solidity ^0.4.17;

import "./TokenAccessor.sol";

contract Identity is TokenAccessor {
    
    event RegisterRequest( address sender, string challenge );
    event LoginRequest( address sender, string challenge );

    constructor() TokenAccessor() public {}

    function register(string challenge) public {
        RegisterRequest(msg.sender, challenge);
        return;
    }

    function login(string challenge) public {
        LoginRequest(msg.sender, challenge);
        token.showInfoFor(msg.sender);
        return;
    }

}