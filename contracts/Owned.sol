pragma solidity ^0.4.17;

contract Owned {
    
    address public owner;
    
    constructor() public {
        owner = msg.sender;
    }
    
    modifier onlyOwner {
        require( msg.sender == owner );
        _;
    }
    
    function transferOwnership( address _newOwner ) public onlyOwner {
        owner = _newOwner;    
    }
}