pragma solidity ^0.4.17;

import "./Owned.sol";

contract HFToken is Owned {
     
    mapping (address => uint256) public balanceOf;
    mapping (address => bool ) public member;
    mapping ( address => bool ) public locked;

    modifier onlyMember {
        require( member[msg.sender] == true );
        _;
    }

    modifier unlocked {
        require( locked[msg.sender] == false );
        _;
    }

    string public name;
    string public symbol;
    uint8 public decimals;

    uint256 public sellPrice;
    uint256 public buyPrice;

    uint256 public totalSupply;

    event Balance(address indexed account, uint256 tokens);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event SellPrice(uint256 price );
    event BuyPrice(uint256 price );

    function HFToken(
        uint256 initialSupply
        ) {
        owner = msg.sender;
        member[owner] = true;
        member[this] = true;
        balanceOf[this] = initialSupply;              // Give the contract all initial tokens
        totalSupply = initialSupply;
        name = "Healthyflow token";
        symbol = "HFT";
        decimals = 2;
        sellPrice = (this.balance / initialSupply) - 10000;
        buyPrice =  this.balance / initialSupply;
    }

    function showInfoFor( address account ) onlyMember {
        
        emit Balance( account, balanceOf[account]);
        emit SellPrice( sellPrice );
        emit BuyPrice( buyPrice );

    }

    function() payable onlyOwner {
        updatePricesByContractState();
    }

    function addMember( address memberAddress ) public onlyOwner {
        member[ memberAddress ] = true;
    }

    function lockAccount( address account ) public onlyMember {
        locked[account] = true;
    }

    function unlockAccount( address account ) public onlyMember {
        locked[account] = false;
    }

    function buy() payable unlocked returns (uint amount) {

        amount = msg.value / buyPrice ;
        require(balanceOf[this] >= amount);
        balanceOf[msg.sender] += amount;
        balanceOf[this] -= amount;
        emit Transfer( this, msg.sender, amount );
        updatePricesByContractState();
        return amount;

    }

    function sell(uint amount) unlocked returns (uint revenue) {

        require( balanceOf[msg.sender] - amount >= 0 );

        balanceOf[this] += amount;
        balanceOf[msg.sender] -= amount;
        revenue = amount * sellPrice;
        msg.sender.transfer(revenue);
        emit Transfer( msg.sender, this, amount );
        updatePricesByContractState();
        return revenue;

    }
    
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {

         _transfer(_from, _to, _value);
        return true;

    }

    function transfer(address _to, uint256 _value) unlocked {
        _transfer(msg.sender, _to, _value);
    }

    function setSellPrice( uint256 value ) onlyOwner {
        sellPrice = value;
        emit SellPrice( value );
    }

    function setBuyPrice( uint256 value ) onlyOwner {
        buyPrice = value;
        emit BuyPrice( value );
    }

    function burn(uint _value) onlyOwner {

        require( balanceOf[this] >= _value );
        
        balanceOf[this] -= _value;
        totalSupply -= _value;
        updatePricesByContractState();

    }

    function fill(uint _value) onlyOwner {

        balanceOf[this] += _value;
        totalSupply += _value;
        updatePricesByContractState();

    }

    function updatePricesByContractState() private {
        sellPrice = ( this.balance / totalSupply ) - 10000;
        emit SellPrice( sellPrice );

        buyPrice = this.balance / totalSupply;
        emit BuyPrice( buyPrice );
    }

    function _transfer(address _from, address _to, uint _value) internal {
        
        require(_to != 0x0);
        require(balanceOf[_from] >= _value);
        require(balanceOf[_to] + _value >= balanceOf[_to]);

        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        
        emit Transfer(_from, _to, _value);  

    }   
}