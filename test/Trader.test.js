/* eslint-disable no-undef */
const assert = require('chai').assert // use Chai Assertion Library
const ganache = require('ganache-cli') // use ganache-cli with ethereum-bridge for Oraclize

// Configure web3 1.0.0 instead of the default version with Truffle
const Web3 = require('web3')
const provider = ganache.provider()
const web3 = new Web3(provider)

// Define the contract we'll be testing
const Trader = artifacts.require('Trader')

// Define tests
contract('Trader', accounts => {
  // define variable to hold the instance of our Template.sol contract
  let template

  // use fresh contract for each test
  beforeEach('Setup contract for each test', async function() {
    template = await Trader.new();
  })

  // check that it sends a query and receives a response
  it('sends a query and receives a response', async function() {
    
    const result = await template.buyRecord( '{message: 20}', 20, accounts[1], {
      from: accounts[0],
      value: web3.utils.toWei('0.000175', 'ether'),
      gas: '3000000',
    })

    let testPassed = false // variable to hold status of result
    for (let i = 0; i < result.logs.length; i++) {
      let log = result.logs[i]
      if (log.event === 'LogRequestSent') {
        console.log("Log request was sent!!");
        console.log(log);
        testPassed = true
      }
    }
    assert(testPassed, '"LogRequestSent" event not found')

    const LogResponseReceived = template.LogResponseReceived()

    let checkForResponse = new Promise((resolve, reject) => {
      LogResponseReceived.watch(async function(error, result) {
        if (error) {
          reject(error)
        }
        const response = await template.response();
        console.log("Response received");
        console.log(response);
      }) 
    })

    const response = await checkForResponse;

    // look for error response

    const LogErrorResponse = template.LogErrorResponse();

    let checkForError = new Promise((resolve, reject) => {
      LogErrorResponse.watch(async function(error, result) {
        if (error) {
          reject(error)
        }
        console.log("Error received");
        console.log(result);
      }) 
    })

    const res1 = await checkForError;

    const LogTransferResponse = template.LogErrorResponse();

    let checkForTransferResponse = new Promise((resolve, reject) => {
      LogTransferResponse.watch(async function(error, result) {
        if (error) {
          reject(error)
        }
        console.log("Transfer event received");
        console.log(result);
      }) 
    })

    const res2 = await checkForTransferResponse;
    
  }) // end 'it' block
}) // end 'contract' block