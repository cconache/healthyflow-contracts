const assert = require('chai').assert
const ganache = require('ganache-cli');

const Web3 = require('web3');
const provider = ganache.provider();
const web3 = new Web3(provider);

// contract we'll be testing
const Identity = artifacts.require('Identity')
const Token = artifacts.require('./HFToken');

// Define tests
contract('Identity', accounts => {

  let identity
  let token
  // use fresh contract for each test
  beforeEach('Setup contract for each test', async function() {
    identity = await Identity.deployed();
    token = await Token.deployed();

    console.log("token address:", token.address );

  })

  // check that on login the contract emits the AccountStats event
  it('responds with the tokens balance event on login', async function(){

    const result = await identity.login("asdasda");

    console.log(result);
    let testPassed = false 

    for (let i = 0; i < result.logs.length; i++) {
      let log = result.logs[i]
      if (log.event === 'TokensBalance') {
        // we found the event
        console.log(log);
        testPassed = true
      }
    }
    
    assert(testPassed, '"TokensBalance" event not found')

  });

  
}) // end 'contract' block