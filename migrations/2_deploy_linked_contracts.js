var HFToken = artifacts.require("./HFToken.sol");
var Identity = artifacts.require("./Identity.sol");
const Trader = artifacts.require('./Trader.sol');

module.exports = async function(deployer,network, accounts) {

  await deployer.deploy(HFToken, 4000000 );  
  let hft = await HFToken.deployed()
  
  await deployer.deploy(Identity);
  await deployer.deploy(Trader);

  console.log("--------Token deployed---------");
  console.log("Token contract address:", hft.address);
  console.log("--------------------------------");
  web3.eth.sendTransaction({to: hft.address, from:  accounts[0], value: 95000000000000000000, gas: 250000});
  
  console.log("--------Link identity to HFToken--------");
  let identity = await Identity.deployed();
  console.log("Identity contract address ",identity.address )
  await identity.setToken( hft.address );
  console.log("Identitiy token address", await identity.token() );

  console.log("--------Link trader to HFToken--------");
  let trader = await Trader.deployed();
  console.log("Trader contract address ",trader.address );
  await trader.setToken( hft.address );
  console.log("Trader token address", await trader.token() );

  console.log("Adding members...");
  await hft.addMember( trader.address );
  await hft.addMember( identity.address );
  console.log("Finished");

};

